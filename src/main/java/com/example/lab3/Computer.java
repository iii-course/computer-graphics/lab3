package com.example.lab3;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

public class Computer {
    public ComputerDimensions dimensions;

    public Rectangle body;
    public Rectangle screen;

    public Rectangle upperStand;
    public Rectangle midStand;
    public Rectangle lowerStand;

    public Rectangle leftButton;
    public Rectangle midButton;
    public Rectangle rightButton;

    public Group face;

    public Group leftArm;
    public Group rightArm;

    private int bodyX;
    private int bodyY;

    private Group leftEye;
    private Group rightEye;
    private QuadCurve smile;

    public Computer(int bodyX, int bodyY) {
        dimensions = new ComputerDimensions(bodyX, bodyY);

        createBody();
        createScreen();

        createStand();
        createButtons();

        createFace();

        createArms();
    }

    private void createBody() {
        int bodyWidth = dimensions.bodyWidth;
        int bodyHeight = dimensions.bodyHeight;

        body = new Rectangle(bodyWidth, bodyHeight);
        body.setX(bodyX);
        body.setY(bodyY);
        body.setFill(Color.WHITE);
        outline(body);
    }

    private void createScreen() {
        int screenWidth = dimensions.screenWidth;
        int screenHeight = dimensions.screenHeight;
        int screenMargin = dimensions.screenMargin;
        screen = new Rectangle(screenWidth, screenHeight);
        screen.setX(bodyX + screenMargin);
        screen.setY(bodyY + screenMargin);
        screen.setFill(Color.LIGHTBLUE);
        outline(screen);
    }

    private void createStand() {
        int bodyHeight = dimensions.bodyHeight;
        int bodyWidth = dimensions.bodyWidth;

        int upperStandWidth = dimensions.upperStandWidth;
        int upperStandHeight = dimensions.upperStandHeight;

        upperStand = new Rectangle(upperStandWidth, upperStandHeight);
        upperStand.setX(bodyX + bodyWidth / 2 - upperStandWidth / 2);
        upperStand.setY(bodyY + bodyHeight);
        upperStand.setFill(Color.WHITE);
        outline(upperStand);

        double midStandWidth = dimensions.midStandWidth;
        double midStandHeight = dimensions.midStandHeight;

        midStand = new Rectangle(midStandWidth, midStandHeight);
        midStand.setY(bodyY + bodyHeight + upperStandHeight);
        midStand.setX(bodyX + bodyWidth / 2 - midStandWidth / 2);
        midStand.setFill(Color.WHITE);
        outline(midStand);

        double lowerStandWidth = dimensions.lowerStandWidth;
        double lowerStandHeight = dimensions.lowerStandHeight;

        lowerStand = new Rectangle(lowerStandWidth, lowerStandHeight);
        lowerStand.setX(bodyX);
        lowerStand.setY(bodyY + bodyHeight + upperStandHeight + midStandHeight);
        lowerStand.setFill(Color.LIGHTBLUE);
        outline(lowerStand);
    }

    private void createButtons() {
        int bodyHeight = dimensions.bodyHeight;
        double buttonWidth = dimensions.buttonWidth;
        double buttonHeight = dimensions.buttonHeight;

        int upperStandHeight = dimensions.upperStandHeight;
        double midStandHeight = dimensions.midStandHeight;
        double lowerStandWidth = dimensions.lowerStandWidth;

        leftButton = new Rectangle(buttonWidth, buttonHeight);
        leftButton.setY(bodyY + bodyHeight + upperStandHeight + midStandHeight + buttonHeight);
        leftButton.setX(bodyX + buttonWidth * 0.5);
        leftButton.setFill(Color.WHITE);
        outline(leftButton);

        midButton = new Rectangle(buttonWidth, buttonHeight);
        midButton.setY(bodyY + bodyHeight + upperStandHeight + midStandHeight + buttonHeight);
        midButton.setX(bodyX + lowerStandWidth - buttonWidth * 3);
        midButton.setFill(Color.WHITE);
        outline(midButton);

        rightButton = new Rectangle(buttonWidth, buttonHeight);
        rightButton.setY(bodyY + bodyHeight + upperStandHeight + midStandHeight + buttonHeight);
        rightButton.setX(bodyX + lowerStandWidth - buttonWidth * 1.5);
        rightButton.setFill(Color.WHITE);
        outline(rightButton);
    }

    private void createFace() {
        createEyes();
        createSmile();
        face = new Group();
        face.getChildren().addAll(leftEye, rightEye, smile);
    }

    private void createEyes() {
        int eyeWidth = dimensions.eyeWidth;
        int eyeHeight = dimensions.eyeHeight;
        int bodyWidth = dimensions.bodyWidth;
        int eyeDistance = dimensions.eyeDistance;
        int eyeCentresY = dimensions.eyeCentresY;

        Ellipse leftEye = new Ellipse(eyeWidth, eyeHeight);
        leftEye.setCenterY(bodyY + eyeCentresY);
        leftEye.setCenterX(bodyX + bodyWidth / 2 - eyeDistance / 2);
        leftEye.setFill(Color.WHITE);
        outline(leftEye);

        Ellipse rightEye = new Ellipse(eyeWidth, eyeHeight);
        rightEye.setCenterY(bodyY + eyeCentresY);
        rightEye.setCenterX(bodyX + bodyWidth / 2 + eyeDistance / 2);
        rightEye.setFill(Color.WHITE);
        outline(rightEye);

        double pupilWidth = dimensions.pupilWidth;
        double pupilHeight = dimensions.pupilHeight;

        Ellipse leftPupil = new Ellipse(pupilWidth, pupilHeight);
        leftPupil.setCenterY(bodyY + eyeCentresY);
        leftPupil.setCenterX(bodyX + bodyWidth / 2 - eyeDistance / 2);
        leftPupil.setFill(Color.BLACK);

        Ellipse rightPupil = new Ellipse(pupilWidth, pupilHeight);
        rightPupil.setCenterY(bodyY + eyeCentresY);
        rightPupil.setCenterX(bodyX + bodyWidth / 2 + eyeDistance / 2);
        rightPupil.setFill(Color.BLACK);

        this.leftEye = new Group();
        this.leftEye.getChildren().addAll(leftEye, leftPupil);

        this.rightEye = new Group();
        this.rightEye.getChildren().addAll(rightEye, rightPupil);
    }

    private void createSmile() {
        int bodyWidth = dimensions.bodyWidth;
        int bodyHeight = dimensions.bodyHeight;

        int smileWidth = dimensions.smileWidth;
        int smileOffsetFromCentre = dimensions.smileOffsetFromCentre;
        int smileDepth = dimensions.smileDepth;

        smile = new QuadCurve();
        smile.setStartX(bodyX + bodyWidth / 2 - smileWidth / 2);
        smile.setStartY(bodyY + bodyHeight / 2 + smileOffsetFromCentre);
        smile.setEndX(bodyX + bodyWidth / 2 + smileWidth / 2);
        smile.setEndY(bodyY + bodyHeight / 2 + smileOffsetFromCentre);
        smile.setControlX(bodyX + bodyWidth / 2);
        smile.setControlY(bodyY + bodyHeight / 2 + smileOffsetFromCentre + smileDepth);
        smile.setFill(Color.WHITE);
        outline(smile);
    }

    private void createArms() {
        int armStartY = dimensions.armStartY;
        int armEndY = dimensions.armEndY;

        int armRightStartX = dimensions.armRightStartX;
        double armRightEndX = dimensions.armRightEndX;

        int armLeftStartX = dimensions.armLeftStartX;
        double armLeftEndX = dimensions.armLeftEndX;

        Line rightArmLine = new Line(
                armRightStartX,
                armStartY,
                armRightEndX,
                armEndY
        );
        rightArmLine.setStroke(Color.BLACK);
        rightArmLine.setStrokeWidth(3);

        Line leftArmLine = new Line(
                armLeftStartX,
                armStartY,
                armLeftEndX,
                armEndY
        );
        leftArmLine.setStroke(Color.BLACK);
        leftArmLine.setStrokeWidth(3);

        this.rightArm = new Group();
        this.rightArm.getChildren().addAll(rightArmLine, createRightPalm(armRightEndX, armEndY));

        this.leftArm = new Group();
        this.leftArm.getChildren().addAll(leftArmLine, createLeftPalm(armLeftEndX, armEndY));
    }

    private Group createLeftPalm(double startX, double startY) {
        return createPalm(
            startX, 
            startY,
            75, 
            180,
            75, 
            270
        );
    }

    private Group createRightPalm(double startX, double startY) {
        return createPalm(
            startX, 
            startY,
            75 + 270, 
            180 + 270,
            75 + 270, 
            270 + 270
        );
    }

    private Group createPalm(
        double startX, 
        double startY, 
        int controlsPalmStartAngle, 
        int controlsPalmEndAngle, 
        int controlsFingersStartAngle, 
        int controlsFingersEndAngle
    ) {
        Group palm = new Group();

        double[][] palmControlPoints = generateControls(
            controlsPalmStartAngle, 
            controlsPalmEndAngle, 10, 
            6,
            startX, 
            startY
        );
        double[][] fignersControlPoints = generateControls(
            controlsFingersStartAngle, 
            controlsFingersEndAngle, 20, 
            10,
            startX, 
            startY
        );
        int fingerControlsIndex = 0;

        for (int i = 0; i < 5; i += 1) {
            CubicCurve finger = new CubicCurve();

            if (i == 0) {
                finger.setStartX(startX);
                finger.setStartY(startY);
            } else {
                finger.setStartX(palmControlPoints[i][0]);
                finger.setStartY(palmControlPoints[i][1]);
            }

            finger.setControlX1(fignersControlPoints[fingerControlsIndex][0]);
            finger.setControlY1(fignersControlPoints[fingerControlsIndex][1]);

            finger.setControlX2(fignersControlPoints[fingerControlsIndex + 1][0]);
            finger.setControlY2(fignersControlPoints[fingerControlsIndex + 1][1]);

            if (i == 4) {
                finger.setEndX(startX);
                finger.setEndY(startY);
            } else {
                finger.setEndX(palmControlPoints[i + 1][0]);
                finger.setEndY(palmControlPoints[i + 1][1]);
            }

            fingerControlsIndex += 1;

            finger.setFill(Color.WHITE);
            outline(finger);

            palm.getChildren().add(finger);
        }

        return palm;
    }

    private void outline(Shape shape) {
        shape.setStroke(Color.BLACK);
        shape.setStrokeWidth(1);
        shape.setStrokeType(StrokeType.CENTERED);
    }

    private double[][] generateControls(
        double startAngle, double endAngle, double radius, int numberOfControls, double centreX, double centreY
    ) {
        double[][] controls = new double[numberOfControls][2];

        double angleStep = (endAngle - startAngle)/ (numberOfControls - 1); 
        double currentAngle = startAngle;

        double currentControlX;
        double currentControlY;

        for (int i = 0; i < numberOfControls; i += 1) {
            currentControlX = toX(centreX, radius, currentAngle);
            currentControlY = toY(centreY, radius, currentAngle);

            controls[i][0] = currentControlX;
            controls[i][1] = currentControlY;

            currentAngle += angleStep;
        }

        return controls;
    }

    private double toX(double centreX, double radius, double angle) {
        return centreX + radius * Math.cos(Math.toRadians(angle));
    }

    private double toY(double centreY, double radius, double angle) {
        return centreY + radius * Math.sin(Math.toRadians(angle));
    }
}
