package com.example.lab3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApplication extends Application {
    private final int windowHeight = 720;
    private final int windowWidth = 1200;

    private ComputerAnimator computerAnimator = null;

    @Override
    public void start(Stage stage) {
        Group computerNode = new Group();

        int computerBodyX = 0;
        int computerBodyY = 0;
        Computer computer = new Computer(computerBodyX, computerBodyY);

        drawComputer(computerNode, computer);
        animateComputer(computerNode, computer);

        Scene scene = new Scene(computerNode, windowWidth, windowHeight);

        stage.setTitle("Lab3");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    private void drawComputer(Group computerNode, Computer computer) {
        computerNode.getChildren().addAll(
                computer.upperStand,
                computer.midStand,
                computer.lowerStand,
                computer.leftButton,
                computer.midButton,
                computer.rightButton,
                computer.leftArm,
                computer.rightArm,
                computer.body,
                computer.screen,
                computer.face
        );
    }

    private void animateComputer(Group computerNode, Computer computer) {
        if (computerAnimator == null) {
            computerAnimator = new ComputerAnimator(computerNode, computer);
        }
        computerAnimator.play();
    }
}