package com.example.lab3;

import javafx.animation.*;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import javafx.util.Duration;

import java.net.URL;
import java.util.Objects;

public class ComputerAnimator {
    private final int pathReadingPrecisionStep = 100;
    private final int animationCycles = 4;
    private final int animationCycleDurationInMs = 20000;

    private final URL trajectoryImageUrl = getClass().getResource("trajectory.png");
    private final Image trajectoryImage = new Image(trajectoryImageUrl.toExternalForm());

    private Group computerNode;
    private Computer computer;

    public ComputerAnimator(Group computerNode, Computer computer) {
        this.computerNode = computerNode;
        this.computer = computer;
    }

    public void play() {
        getRotateTransition(computer.leftArm, 0, 30).play();
        getRotateTransition(computer.rightArm, 0,-30).play();
        getRotateTransition(computer.face, -5,5).play();
        getStrokeTransition(computer.leftButton).play();
        getParallelTransition(computerNode).play();
    }

    private ParallelTransition getParallelTransition(Node node) {
        PathTransition pathTransition = getPathTransition(node);
        FadeTransition fadeTransition = getFadeTransition(node);
        ScaleTransition scaleTransition = getScaleTransition(node);

        ParallelTransition parallelTransition = new ParallelTransition(
                node,
                pathTransition,
                fadeTransition,
                scaleTransition
        );

        parallelTransition.setCycleCount(animationCycles);
        parallelTransition.setAutoReverse(true);

        return parallelTransition;
    }

    private PathTransition getPathTransition(Node node) {
        PathTransition pathTransition = new PathTransition();

        pathTransition.setDuration(Duration.millis(animationCycleDurationInMs));
        pathTransition.setNode(node);
        pathTransition.setPath(buildPath());
        pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pathTransition.setCycleCount(1);

        return pathTransition;
    }

    private FadeTransition getFadeTransition(Node node) {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(animationCycleDurationInMs), node);

        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1.0);
        fadeTransition.setCycleCount(1);

        return fadeTransition;
    }

    private ScaleTransition getScaleTransition(Node node) {
        ScaleTransition scaleTransition = new ScaleTransition(Duration.millis(animationCycleDurationInMs), node);

        scaleTransition.setByX(2);
        scaleTransition.setByY(2);
        scaleTransition.setAutoReverse(true);

        return scaleTransition;
    }

    private RotateTransition getRotateTransition(Node node, double fromAngle, double toAngle) {
        final int cycleCount = 8 * animationCycles;

        RotateTransition rotateTransition = new RotateTransition(
                Duration.millis(animationCycleDurationInMs * animationCycles / cycleCount),
                node
        );
        rotateTransition.setFromAngle(fromAngle);
        rotateTransition.setToAngle(toAngle);
        rotateTransition.setCycleCount(cycleCount);
        rotateTransition.setAutoReverse(true);

        return rotateTransition;
    }

    private StrokeTransition getStrokeTransition(Shape shape) {
        final int cycleCount = 15 * animationCycles;
        StrokeTransition strokeTransition = new StrokeTransition(
                Duration.millis(animationCycleDurationInMs * animationCycles / cycleCount),
                shape,
                Color.BLACK,
                Color.RED
        );
        strokeTransition.setCycleCount(cycleCount);
        strokeTransition.setAutoReverse(true);
        return strokeTransition;
    }

    private Path buildPath() {
        Path path = new Path();
        PixelReader pixelReader = trajectoryImage.getPixelReader();

        int endX = -1, endY = -1,
                controlX1 = -1, controlY1 = -1,
                controlX2 = -1, controlY2 = -1;

        boolean isPathInitialized = false;

        for (int readX = 0; readX < trajectoryImage.getWidth(); readX += pathReadingPrecisionStep) {
            for (int readY = 0; readY < trajectoryImage.getHeight(); readY++) {
                Color color = pixelReader.getColor(readX, readY);

                if (Objects.equals(color.toString(), Color.BLACK.toString())) {
                    if (!isPathInitialized) {
                        isPathInitialized = true;
                        path.getElements().add(new MoveTo(readX, readY));
                    }

                    if (controlX1 < 0) {
                        controlX1 = readX;
                        controlY1 = readY;
                    } else if (controlX2 < 0) {
                        controlX2 = readX;
                        controlY2 = readY;
                    } else if (endX < 0) {
                        endX = readX;
                        endY = readY;
                    } else {
                        CubicCurveTo cubicCurve = new CubicCurveTo(
                                controlX1,
                                controlY1,
                                controlX2,
                                controlY2,
                                endX,
                                endY
                        );

                        controlX1 = endX;
                        controlY1 = endY;
                        endX = -1;
                        endY = -1;
                        controlX2 = -1;
                        controlY2 = -1;

                        path.getElements().add(cubicCurve);
                    }
                }
            }
        }

        return path;
    }
}
