package com.example.lab3;

class ComputerDimensions {
    public final int bodyHeight = 75;
    public final int bodyWidth = 100;

    public int screenMargin;
    public int screenHeight;
    public int screenWidth;

    public int upperStandWidth;
    public int upperStandHeight;

    public double midStandWidth;
    public double midStandHeight;

    public double lowerStandWidth;
    public double lowerStandHeight;

    public double buttonWidth;
    public double buttonHeight;

    public int eyeHeight;
    public int eyeWidth;

    public double pupilHeight;
    public double pupilWidth;

    public int eyeDistance;
    public int eyeCentresY;

    public int smileDepth;
    public int smileWidth;
    public int smileOffsetFromCentre;

    public final int armAngle = 45;
    public int armLength;
    public double armBodyOffset;

    public int handHeight = bodyHeight / 8;
    public int handWidth = handHeight / 2;

    public int armStartY;
    public int armEndY;

    public int armRightStartX;
    public double armRightEndX;

    public int armLeftStartX;
    public double armLeftEndX;

    public int bodyX;
    public int bodyY;

    public ComputerDimensions(int bodyX, int bodyY) {
        this.bodyX = bodyX;
        this.bodyY = bodyY;
        recalculateDimensions();
    }

    public void recalculateDimensions() {
        screenMargin = bodyHeight / 10;
        screenHeight = bodyHeight - screenMargin * 2;
        screenWidth = bodyWidth - screenMargin * 2;

        upperStandWidth = bodyWidth / 2;
        upperStandHeight = bodyHeight/ 6;

        midStandWidth = bodyWidth / 1.5;
        midStandHeight = bodyHeight / 10;

        lowerStandWidth = bodyWidth;
        lowerStandHeight = bodyHeight / 3;

        buttonWidth = lowerStandWidth / 6;
        buttonHeight = lowerStandHeight / 8;

        eyeHeight = screenHeight / 10;
        eyeWidth = screenWidth / 20;

        pupilHeight = eyeHeight / 1.5;
        pupilWidth = eyeWidth / 1.5;

        eyeDistance = screenWidth / 3;
        eyeCentresY = screenMargin + eyeHeight * 4;

        smileDepth = screenHeight / 7;
        smileWidth = screenWidth / 3;
        smileOffsetFromCentre = eyeHeight * 2;

        armLength = bodyHeight / 3;
        armBodyOffset = armLength * Math.sin(armAngle);

        handHeight = bodyHeight / 8;
        handWidth = handHeight / 2;

        armStartY = bodyY + bodyHeight / 2;
        armEndY = bodyY + bodyHeight / 2 + armLength;

        armRightStartX = bodyX + bodyWidth;
        armRightEndX = bodyX + bodyWidth + armBodyOffset;

        armLeftStartX = bodyX;
        armLeftEndX = bodyX - armBodyOffset;
    }
}
